def Messenger
  attr_reader :endpoint, :payload

  def initialize(endpoint, payload)
    @endpoint = endpoint
    @payload = payload
  end

  def call
    return RestClient.post( endpoint, payload, { content_type: :json, accept: :json } ).response.code
  end

end
