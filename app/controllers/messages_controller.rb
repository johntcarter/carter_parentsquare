class MessagesController < ApplicationController

  def index
    @messages = Message.order(created_at: 'desc')
  end

  def receive
    message = Message.create(
               to_number:    params[:to_number],
               content:      params[:message]
             )

    if message.valid?
      if message.valid_number?
        message.attempt_text
      else
        # Make it here if the phone number has previously been listed as an invalid number
        message.invalidate
      end
    # else
    #  make it here if the initial api call is invalid (either missing to_number or content)
    end
  end

  def callback
    message = Message.find( params[:message_id] )

    if message.present?
      case params[:status]
      when "delivered"
        message.deliver!
      when "failed"
        message.fail!
      when "invalid"
        message.invalidate!
      else
        # My instinct is to mark it as invalid if an incorrect status is sent
      end
    # else
    # make it here if invalid message_id sent
    end
  end

end
