# == Schema ==
#
# id         integer
# url        string
# weight     text
# calls      string
#

class Endpoint < ApplicationRecord

  validates :weight, numericality: { greater_than: 0 }

  # this will return an array with the two saved endpoints
  # in the order of which should be attempted first
  # the calculations in this runs a weighted round robin
  # by tracking the existing calls to the endpoint and
  # recalculating the weight on every call (should it ever
  # be changed). The bulk of this logic was grabbed
  # from stackoverflow
  # (https://stackoverflow.com/questions/42990058/ruby-weighted-round-robin)
  # but obviously customized. the succ and fdiv methods
  # were new ones to me
  def self.calculate
    endpoint0 = Endpoint.all[0]
    endpoint1 = Endpoint.all[1]
    ratio = endpoint0.weight.fdiv endpoint1

    r0 = endpoint0.calls.succ.fdiv endpoint1.calls
    r1 = endpoint1.calls.succ.fdiv endpoint0.calls

    if (r0 - ratio).abs <= (r1 - ratio).abs
      return [endpoint0, endpoint1]
    else
      return [endpoint1, endpoint0]
    end
  end

  def increment_calls
    update_attributes(calls: calls + 1)
  end

end
