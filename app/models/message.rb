# == Schema ==
#
# id            integer
# to_number     string
# content       text
# status        string
#

class Message < ApplicationRecord
  
  STATUS_TYPES = [ "delivered", "failed", "invalid", "pending" ].freeze

  validates_inclusion_of :status, in: STATUS_TYPES
  validates_presence_of :to_number
  validates_presence_of :content

  def fail!
    update_attributes(status: "failed")
  end

  def deliver!
    update_attributes(status: "delivered")
  end

  def invalidate!
    update_attributes(status: "invalid")
  end

  # Check to see if the number has already been flagged in our system as invalid
  # Based off of the size of the database, I might create a separate mechanism for
  # this where we blacklist certain numbers instead of searching ALL messages,
  # but in trying to not have feature creep with the current implementation, I left
  # it as it was
  def valid_number?
    Message.where(to_number: to_number, status: "invalid").blank?
  end

  def attempt_text
    # provides an array with two endpoints in the order they should be attempted
    endpoints = Endpoint.calculate

    callback_url = Rails.application.routes.url_helpers.product_url(self) + "/api/messages/callback"

    payload = { to_number: to_number, message: content, callback_url: callback_url }

    # The following code feels more than a little clunky but I also am okay with it because
    # for something like making these codes I'm okay with seeing the explicit code. On a code
    # review, I'd be open to cleaning it up.

    response_code = Messenger.new( endpoints[0], payload ).call

    # if the service doesn't work, try the next provided
    if response_code >= 500
      response = Messenger.new( endpoints[1], payload ).call

      if response_code >= 500
        # send a report that the both endpoints are down
      else
        # successfully touched this endpoint, so increment the call count
        endpoints[1].increment_calls
      end
    else
      # successfully touched this endpoint, so increment the call count
      endpoints[0].increment_calls
    end
  end

end
