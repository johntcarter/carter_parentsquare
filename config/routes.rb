Rails.application.routes.draw do
  root to: 'messages#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/api/message/receive', to: 'messages#receive'
  post '/api/message/callback', to: 'messages#callback'
end
