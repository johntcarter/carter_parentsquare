# Carter PartnerSquare

This is the code base for the Texting Service project

I created two models for this project: a Message model and an Endpoint model
The Message model is just as you'd expect, it has a `to_number`, `content`
(because I felt weird having a message model with a message attr), and a
`status` attribute that is restricted to what can be there. It defaults to
`pending` and I was thinking there could be a cron that runs regularly to look
for `pending` messages with a certain age (five minutes or whatever) so that we
could programmatically tag them as failed or invalid as the callback was never made back.

The Endpoint model was my answer to how to handle the Weighted Round Robin requested.
At first, I was thinking of tagging the Messages with their endpoint, but that felt
like majorly bad data management for a lot of reasons. Plus, to calculate that the
correct weight is being managed we'd have to grab ALL messages so it seemed like
something we should normalize instead. By making them objects, it also makes
configuring of the weight next to trivial.

Truth be told, I wasn't able to really fully test this as I didn't have time to set up the
multiple endpoints so there might be errors lurking around (I sometimes have a way with tpyos)
but feel that for the most point it should work and was proud of it. I tried
to comment rigorously and am open to any questions or comments the team might
have. Thanks for taking the time to review this!
