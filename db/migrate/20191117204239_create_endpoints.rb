class CreateEndpoints < ActiveRecord::Migration[5.2]
  def change
    create_table :endpoints do |t|
      t.string  :url, null: false
      t.integer :weight, default: 1
      t.integer :calls, default: 0

      t.timestamps
    end
  end
end
