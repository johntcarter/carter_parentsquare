class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.string :to_number, null: false
      t.text :content, null: false
      t.string :status, default: "pending"

      t.timestamps
    end
  end
end
